//
// Created by kali on 4/2/20.
//

#ifndef ZIRAXAN_UTILS_H
#define ZIRAXAN_UTILS_H

#include <algorithm>
#include <vector>
#include <string>
#include <cctype>

std::vector<std::string> explode(std::string, int);

auto index(const std::string&, const std::string&) -> int;

auto count(std::string, std::string) -> int;

std::vector<std::string> gen_split(std::string, const std::string&, int, bool debug=false);

std::vector<std::string> split(std::string, const std::string&, bool debug=false);

std::string trim_space(std::string);

std::vector<std::string> split_trim(std::string s, const std::string& sep, bool debug=false);

bool is_number(const std::string&);

#endif //ZIRAXAN_UTILS_H
