//
// Created by kali on 4/2/20.
//

#ifndef ZIRAXAN_TYPES_H
#define ZIRAXAN_TYPES_H

#include <vector>
#include <string>

#define CMD_QUIT "quit"
#define CMD_SORT_DESCENDING "descending"
#define CMD_SORT_ASCENDING "ascending"
#define CMD_PROCESSES "processes"
#define CMD_DIR "dir"
#define CMD_DELIMITER "-"
#define CMD_EQUAL "="

#define BUF_MAX_SIZE 8192

#define SORT_START "sort_start"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

typedef struct command_t command_t;

class Presenter {
public:
    explicit Presenter(std::string);
    ~Presenter() = default;

    auto run() -> int;
private:
    void wait();

    std::string fifo_name;
};

class Worker {
public:
    explicit Worker(int, std::string);
    ~Worker() = default;
    void set_pid(pid_t _pid) { pid = _pid; }
    pid_t get_pid() { return pid; }
    auto run() -> int;

private:
    std::string get_prefix();

    pid_t pid;
    int pipe_fd;
    std::string file_name;
    std::string fifo_name;
    std::vector<std::vector<std::string> > filter_params;
};

struct command_t {
    bool quit;
    std::vector<std::vector<std::string> > filter_params;
    std::vector<std::string> sort_params;
    uint n_processes;
    std::string dir;
};

class Cli {
public:
    Cli ();
    ~Cli () = default;

    auto get_command() -> command_t { return *command; }

    void wait();

private:
    auto parse(std::string&) -> int;
    auto verify() -> int;
    void reset();
    command_t *command;
};

class Ziraxan {
public:
    Ziraxan (): cli(nullptr), presenter(nullptr) {}
    ~Ziraxan ();
    auto init() -> int;
    auto run () -> int;

private:
    Cli* cli;
    std::vector<Worker*> workers;
    Presenter* presenter;
};

#endif //ZIRAXAN_TYPES_H
