//
// Created by kali on 4/4/20.
//

#include <iostream>
#include "string/utils.h"
using namespace std;
// explode splits s into a slice of strings,
// one string per character up to a maximum of n (n < 0 means no limit).
vector<string> explode(string s, int n) {
    auto l = s.length();
    if (n < 0 || n > l) {
        n = l;
    }
    vector<string> a(n);
    for (auto i = 0; i < n-1; ++i) {
        a[i] = s[i];
        s = s.substr(i+1);
    }
    if (n > 0) {
        a[n - 1] = s;
    }
    return a;
}

// index returns the index of the first instance of substr in s, or -1 if substr is not present in s.
auto index(const string& s, const string& substr) -> int {
    auto found = s.find(substr);
    if (found == string::npos) {
        return -1;
    }
    return found;
}

// count counts the number of non-overlapping instance of substr in s.
// If substr is an empty string, count returns 1 + the number of points in s.
auto count(string s, string substr) -> int {
    if (substr.empty()) {
        return (int) s.size() + 1;
    }

    auto n = 0;
    while (true) {
        auto i = index(s, substr);
        if (i == -1) {
            return n;
        }
        ++n;
        s = s.substr(i+substr.size());
    }
}

// generic split: splits after each instance of sep
vector<string> gen_split(string s, const string& sep, int n, bool debug) {
    if (n == 0) {
        return vector<string>(0);
    }
    if (sep.empty()) {
        return explode(s, n);
    }
    if (n < 0) {
        n = count(s, sep) + 1;
    }
    if (debug) {
        std::cout << n - 1 << endl;
        std::cout << "$" << s << "$" << endl;
    }

    vector<string> a(n);
    --n;
    auto i = 0;
    while (i < n) {
        auto m = index(s, sep);
        if (m < 0) {
            break;
        }
        a[i] = s.substr(0, m);
//        if (debug) {
//            cout << a[i] << " $ ";
//        }
        s = s.substr(m+sep.size());
        ++i;
    }
//    if (debug) {
//        cout << endl;
//    }
    a[i] = s;
    return a;
}

vector<string> split(string s, const string& sep, bool debug) {
    return gen_split(s, sep, -1, debug);
}

// trim_space returns a slice of the string s, with all leading
// and trailing white space removed.
string trim_space(string s) {
    while (isspace(s[0])) {
        s.erase(0, 1);
    }

    while (isspace(s[s.size()-1])) {
        s.erase(s.size()-1, 1);
    }

    return s;
}

vector<string> split_trim(string s, const string& sep, bool debug) {
    auto res = split(s, sep, debug);
    for (auto & t : res) {
        t = trim_space(t);
    }
    return res;
}

bool is_number(const string& s)
{
    return !s.empty() && find_if(s.begin(),
                                      s.end(), [](unsigned char c) { return !isdigit(c); }) == s.end();
}
