//
// Created by kali on 4/4/20.
//

#include <iostream>
#include <fstream>

#include <unistd.h>
#include <fcntl.h>
#include "string/utils.h"
#include "types.h"
using namespace std;

Worker::Worker(int _fd, string _name) {
    pid = 0;
    pipe_fd = _fd;
    fifo_name = _name;
}

auto Worker::run() -> int {

    // read file name and filter params
    auto buffer = new char[BUF_MAX_SIZE];
    buffer[0] = '\0';
    auto n_bytes_read = read(pipe_fd, buffer, BUF_MAX_SIZE);
    if (n_bytes_read < 1) {
        cout << get_prefix() << "error getting file name and filters." << endl;
        return -1;
    }
    buffer[n_bytes_read] = '\0';
    auto params = string(buffer);
    auto tokens = split(params, ",");
    file_name = tokens[0];

    for (int i = 1; i < tokens.size(); i+=2) {
        vector<string> filter;
        filter.push_back(tokens[i]);
        filter.push_back(tokens[i+1]);
        filter_params.push_back(filter);
    }

    // start reading file and save filtered records
    ifstream infile(file_name);
    // read header
    vector<string> header;
    string line;

    getline(infile, line);
    header = split_trim(line, "-");

    // find filter params indices
    for (int j = 0; j < filter_params.size(); ++j) {
        auto filter_name = filter_params[j][0];
        auto found = false;
        for (int i = 0; i < header.size(); ++i) {
            if (header[i] == filter_name) {
                filter_params[j][0] = to_string(i);
                found = true;
                break;
            }
        }
        if (!found) {
            cout << get_prefix() << "filter " << filter_params[j][0] << " = " << filter_params[j][1] << " is not applicable." << endl;
            filter_params.erase(filter_params.begin() + j);
        }
    }

    // start filtering
    bool keep = true;
    vector<vector<string> > result;
    while(getline(infile, line)) {
        keep = true;
        auto new_record = split_trim(line, "-");
        for (auto & filter : filter_params) {
            auto idx = stoul(filter[0]);
            if (new_record[idx] != filter[1]) {
                keep = false;
                break;
            }
        }

        if (keep) {
            auto fd = open(fifo_name.c_str(), O_WRONLY);
            if (fd < 0 ) {
                cout << get_prefix() << "error opening fifo." << endl;
                return -1;
            }
            line += '\n';
            write(fd, line.c_str(), line.size());
            close(fd);
        }
    }

    infile.close();

    return 0;
}

string Worker::get_prefix() {
    return string("worker[") + to_string(pid) + string("]: ");
}