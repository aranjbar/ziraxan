//
// Created by kali on 4/2/20.
//

#include <new>
#include <iostream>
#include <algorithm>
#include <fstream>

#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <glob.h>
#include "types.h"
#include "string/utils.h"
using namespace std;

auto random_string(int) -> string;
vector<string> glob_vector(const string&);

Ziraxan::~Ziraxan() {
    // destructor
    delete cli;
}

auto Ziraxan::init() -> int {
    auto _cli = new (nothrow) Cli();
    if (!_cli) {
        return -1;
    }
    cli = _cli;

    return 0;
}

int Ziraxan::run() {

    while (true) {
        cli->wait();

        auto command = cli->get_command();
        if (command.quit) {
            break;
        }
        string files_dir = command.dir + ((command.dir[command.dir.size()-1] == '/')? string("*"): string("/*"));
        auto files = glob_vector(files_dir);
        auto n_active_workers = 0;
        // create named pipe
        auto fifo_name = string("/tmp/fifo") + random_string(5);
        mkfifo(fifo_name.c_str(), 0666);

        // create presenter
        presenter = new Presenter(fifo_name);
        if (fork() == 0) {
            // run presenter
            presenter->run();
            exit(0);
        }
        auto fifo_fd = open(fifo_name.c_str(), O_WRONLY);
        if (fifo_fd < 0) {
            cout << "load balancer: error opening fifo" << endl;
        }
        if (!command.sort_params.empty()) {
            auto params = command.sort_params[0] + "," + command.sort_params[1];
            auto n_bytes_write = write(fifo_fd, params.c_str(), params.size());
            close(fifo_fd);
            if (n_bytes_write < 1) {
                cout << "load balancer: error sending sort values." << endl;
            }
        }

        // a file for reading header
        string f;

        while (!files.empty()) {

            while (n_active_workers < command.n_processes) {

                if (files.empty()) {
                    break;
                }
                auto file = files.back();
                f = file;
                files.pop_back();
                auto params = file + ",";
                for (int i = 0; i < command.filter_params.size(); ++i) {
                    auto filter = command.filter_params[i];
                    if (i == command.filter_params.size() - 1) {
                        params += filter[0] + "," + filter[1];
                    } else {
                        params += filter[0] + "," + filter[1] + ",";
                    }
                }
                // new worker
                int fd[2];
                if (pipe(fd) == -1) {
                    cout << "load balancer: pipe creating error" << endl;
                }
                auto worker = new Worker(fd[0], fifo_name);
                auto pid = fork();

                if (pid < 0) {
                    cout << "load balancer: fork error." << endl;
                } else if (pid > 0) {
                    worker->set_pid(pid);
                    close(fd[0]);
                    auto n_bytes_write = write(fd[1], params.c_str(), params.size());
                    if (n_bytes_write < 1) {
                        cout << "load balancer: error sending information to worker[" << worker->get_pid() << "]" << endl;
                    }
                    close(fd[1]);
                    workers.push_back(worker);
                } else {
                    worker->set_pid(getpid());
                    close(fd[1]);
                    auto err = worker->run();
                    close(fd[0]);
                    exit(err);
                }

                ++n_active_workers;
            }
            auto pid = wait(NULL);
            // find worker by pid and destruct it.
            auto worker_idx = find_if(workers.begin(), workers.end(), [&pid](Worker *w) { return w->get_pid() == pid; });
            if (worker_idx != workers.end()) {
                delete workers.at(distance(workers.begin(), worker_idx));
                workers.erase(worker_idx);
                --n_active_workers;
            }
        }
        // wait for remaining processes
        for (int i = 0; i < n_active_workers; ++i) {
            if (wait(NULL) != -1) {
                --n_active_workers;
                --i;
            }
        }

        // read header and send it to presenter
        ifstream infile(f);
        vector<string> header;
        string line;

        getline(infile, line);
        line = "header:" + line;
        fifo_fd = open(fifo_name.c_str(), O_WRONLY);
        if (fifo_fd < 0) {
            cout << "load balancer: error opening fifo" << endl;
        }
        auto n_bytes_write = write(fifo_fd, line.c_str(), line.size());
        close(fifo_fd);
        if (n_bytes_write < 1) {
            cout << "load balancer: error sending header." << endl;
        }

        infile.close();
        delete presenter;
    }

    return 0;
}

auto random_string(int len) -> string {
    uint i = 0, char_idx = 0;
    string charset = "0123456789AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz";
    string rand_str;

    srandom(time(NULL));
    for (i = 0; i < len; ++i) {
        char_idx = random()%charset.size();
        rand_str.push_back(charset[char_idx]);
    }
    return rand_str;
}

vector<string> glob_vector(const string& pattern) {
    glob_t glob_result;
    glob(pattern.c_str(), GLOB_TILDE, NULL, &glob_result);
    vector<string> files;
    for (int i = 0; i < glob_result.gl_pathc; ++i) {
        files.push_back(string(glob_result.gl_pathv[i]));
    }
    globfree(&glob_result);
    return files;
}
