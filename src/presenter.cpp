//
// Created by kali on 4/3/20.
//

#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <fcntl.h>

#include "types.h"
#include "string/utils.h"
using namespace std;

Presenter::Presenter(string _name) {
    fifo_name = _name;
}

auto sort_idx = 0;
auto sort_a = 1;

auto Presenter::run() -> int {
    auto fd = open(fifo_name.c_str(), O_RDONLY);
    if (fd < 0) {
        cout << "presenter: cannot open fifo." << endl;
        return -1;
    }

    // read sort values
    char buffer[BUF_MAX_SIZE];
    buffer[0] = '\0';
    auto n_bytes_read = read(fd, buffer, BUF_MAX_SIZE);
    close(fd);
    if (n_bytes_read < 1) {
        cout << "presenter: error getting sort values." << endl;
    }
    buffer[n_bytes_read] = '\0';
    auto line = string(buffer);
    auto sort_params = split_trim(line, ",");



    // read input records
    vector<vector<string> > records;
    vector<string> header;
    vector<string> record;
    fd = open(fifo_name.c_str(), O_RDONLY);
    if (fd < 0) {
        cout << "presenter: cannot open fifo." << endl;
        return -1;
    }

    auto done = false;
    while(true) {
        buffer[0] = '\0';
        n_bytes_read = read(fd, buffer, BUF_MAX_SIZE);
        if (n_bytes_read < 1) {
            continue;
        }
        buffer[n_bytes_read] = '\0';
        auto buff_str = string(buffer);
        auto lines = split(buff_str, "\n");
        for (auto l : lines) {
            if (l.empty()) {
                continue;
            }
            if (l.substr(0, 7) == "header:") {
                l = l.substr(7);
                header = split_trim(l, "-");
                done = true;
            } else {
                record.clear();
                record = split_trim(l, "-");
                records.push_back(record);
            }
        }
        if (done) {
            break;
        }
    }
    close(fd);


    for (int i = 0; i < header.size(); ++i) {
        if (header[i] == sort_params[0]) {
            sort_idx = i;
        }
    }
    cout << endl;
    cout << setw(40) << header[0] << setw(10) << header[1];
    cout << setw(10) << header[2] << setw(19) << header[3];
    cout << setw(40) << header[4] << setw(10) << header[5];
    cout << setw(10) << header[6] << setw(12) << header[7];
    cout << setw(13) << header[8] << setw(13) << header[9];
    cout << endl;

    if (sort_params[1] == CMD_SORT_DESCENDING) {
        sort_a = 0;
    }

    sort(records.begin(), records.end(), [](const vector<string>& a, const vector<string>& b) {
        if (sort_a) {
            if (sort_idx == 2 || sort_idx == 5 || sort_idx == 6 || sort_idx == 7 || sort_idx == 8 || sort_idx == 9) {
                return stof(a[sort_idx]) < stof(b[sort_idx]);
            } else {
                return a[sort_idx] < b[sort_idx];
            }
        } else {
            if (sort_idx == 2 || sort_idx == 5 || sort_idx == 6 || sort_idx == 7 || sort_idx == 8 || sort_idx == 9) {
                return stof(a[sort_idx]) > stof(b[sort_idx]);
            } else {
                return a[sort_idx] > b[sort_idx];
            }
        }
    });

    for (auto & r : records) {
        cout << setw(40) << r[0] << setw(7) << r[1];
        cout << setw(13) << r[2] << setw(20) << r[3];
        cout << setw(40) << r[4] << setw(7) << r[5];
        cout << setw(10) << r[6] << setw(13) << r[7];
        cout << setw(10) << r[8] << setw(12) << r[9];
        cout << endl;
    }

    close(fd);

    return 0;
}

void Presenter::wait() {

}