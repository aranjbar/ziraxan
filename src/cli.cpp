//
// Created by kali on 4/2/20.
//

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <sys/stat.h>

#include "types.h"
#include "string/utils.h"

using namespace std;

Cli::Cli() {
    command = new command_t;
    command->quit = false;
    command->n_processes = 0;
    command->dir = "";
}

void Cli::wait() {
    reset();
    string cmd_str;
    int err = -1;

    while(cout << "> " && getline(cin, cmd_str)) {
        if (cmd_str.empty()) {
            continue;
        }

        err = parse(cmd_str);
        if (err == -1) {
            continue;
        }
        err = verify();
        if (err == -1) {
            reset();
            continue;
        }
        break;
    }

    if (err == -1) {
        exit(EXIT_FAILURE);
    }
}

auto Cli::parse(string& cmd_str) -> int {
    string token;
    bool delimiter_required = false;

    if (cmd_str == CMD_QUIT) {
        command->quit = true;
        return 0;
    }

    istringstream iss(cmd_str);
    while(iss >> token) {

        if (delimiter_required && token != CMD_DELIMITER) {
            cout << "command format error at " << token << endl;
            return -1;
        }

        if (token == CMD_PROCESSES) {
            token = "";
            iss >> token;
            if (token != CMD_EQUAL) {
                cout << "command format error at " << token << endl;
                return -1;
            }
            token = "";
            iss >> token;
            if (!is_number(token)) {
                cout << "command format error at " << token << endl;
                return -1;
            }
            command->n_processes = stoul(token);
            delimiter_required = true;
        } else if (token == CMD_DIR) {
            token = "";
            iss >> token;
            if (token != CMD_EQUAL) {
                cout << "command format error at " << token << endl;
                return -1;
            }
            token = "";
            iss >> token;
            if (token.empty()) {
                cout << "command format error at " << token << endl;
                return -1;
            }
            command->dir = token;
            delimiter_required = true;
        } else if (token == CMD_DELIMITER) {
            if (!delimiter_required) {
                cout << "command format error at " << token << endl;
                return -1;
            }
            delimiter_required = false;
        } else {
            string key, value;
            key = token;
            token = "";
            iss >> token;
            if (token != CMD_EQUAL) {
                cout << "command format error at " << token << endl;
                return -1;
            }
            token = "";
            iss >> token;
            if (token.empty()) {
                cout << "command format error at " << token << endl;
                return -1;
            }
            value = token;

            if (value == CMD_SORT_ASCENDING || value == CMD_SORT_DESCENDING) {
                command->sort_params.push_back(key);
                command->sort_params.push_back(value);
            } else {
                vector<string> filter;
                filter.push_back(key);
                filter.push_back(value);
                command->filter_params.push_back(filter);
            }
            delimiter_required = true;
        }
    }

    return 0;
}

auto Cli::verify() -> int {
    if (command->quit) {
        return 0;
    }
    if (command->n_processes < 1) {
        cout << "number of processes must grater than 1." << endl;
        return -1;
    }
    struct stat dir_stat{};
    if (stat(command->dir.c_str(), &dir_stat) != 0) {
        cout << command->dir << ": error" << endl;
        return -1;
    }
    if (!S_ISDIR(dir_stat.st_mode)) {
        cout << command->dir << " is not a directory." << endl;
        return -1;
    }
    return 0;
}

void Cli::reset() {
    command->quit = false;
    command->n_processes = 0;
    command->dir = "";
    for (auto &filter_param : command->filter_params) {
        filter_param.clear();
    }
    command->filter_params.clear();
    command->sort_params.clear();
}