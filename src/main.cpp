//
// Created by kali on 4/2/20.
//

#include <new>
#include <ctime>
#include <cstdlib>

#include "types.h"
using namespace std;

int main (int argc, char **argv) {
    const time_t proc_start = time (NULL);

    // ziraxan main context
    auto ziraxan_ctx = new (nothrow) Ziraxan ();
    if (!ziraxan_ctx) {
        exit (EXIT_FAILURE);
    }

    if (ziraxan_ctx->init() == -1) {
        delete ziraxan_ctx;
        return -1;
    }

    // TODO: welcome_screen();

    auto err = ziraxan_ctx->run();
    // finished with ziraxan, clean up
    const time_t proc_stop = time (NULL);
    // TODO: goodbye_screen(proc_start, proc_stop);

    delete ziraxan_ctx;

    return err;
}